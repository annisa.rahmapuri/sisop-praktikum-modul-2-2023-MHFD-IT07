#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>
#include <sys/wait.h>
#include <fnmatch.h>

#define MANUTD_TEAM "ManUtd"
#define MAX_FILENAME 256

void download_Data();
void extract_File();
void remove_non_manchester_united_players();
void sort_player();
void make_team(int bek, int gelandang, int striker);

int main() {
    download_Data();
    extract_File();
    remove_non_manchester_united_players();
    sort_player();
    
    int bek, gelandang, striker;
    printf("Masukkan jumlah bek: ");
    scanf("%d", &bek);
    printf("Masukkan jumlah gelandang: ");
    scanf("%d", &gelandang);
    printf("Masukkan jumlah striker: ");
    scanf("%d", &striker);
  
    make_team(bek, gelandang, striker);
    return 0;
}

void download_Data() {
    char* args[] = {"curl", "-L", "-o", "players.zip", "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", NULL};
    pid_t pid = fork();

    if (pid == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
    } else if (pid == 0) { // child process
        execvp(args[0], args);
        perror("execvp");
        exit(EXIT_FAILURE);
    } else { // parent process
        int status;
        waitpid(pid, &status, 0);
        if (WIFEXITED(status) && WEXITSTATUS(status) != 0) {
            printf("Error downloading players\n");
            exit(EXIT_FAILURE);
        }
    }
}

void extract_File() {
    char* args[] = {"unzip", "players.zip", NULL};
    pid_t pid = fork();

    if (pid == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
    } else if (pid == 0) { // child process
        execvp(args[0], args);
        perror("execvp");
        exit(EXIT_FAILURE);
    } else { // parent process
        int status;
        waitpid(pid, &status, 0);
        if (WIFEXITED(status) && WEXITSTATUS(status) != 0) {
            printf("Error extracting players\n");
            exit(EXIT_FAILURE);
        }
    }

    if (remove("players.zip") != 0) {
        printf("Error deleting file: players.zip\n");
    }
}

void remove_non_manchester_united_players() {
    DIR* dir = opendir("players");
    struct dirent* entry;
    char fileName[MAX_FILENAME];

    if (dir == NULL) {
        printf("Error opening directory\n");
        exit(EXIT_FAILURE);
    }

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) { // jika entri adalah file biasa
            if (fnmatch("*_ManUtd_*_*.png", entry->d_name, 0) != 0) {
                sprintf(fileName, "%s/%s", "players", entry->d_name);
                if (unlink(fileName) != 0) {
                    printf("Error deleting file: %s\n", fileName);
                }
            }
        }
    }

    closedir(dir);
}

void sort_player() {
    DIR * dir = opendir("./players");
    struct dirent * entry;
    char * image_file;
    char * positions[] = {
      "Kiper",
      "Bek",
      "Gelandang",
      "Penyerang"
    };

    if (dir == NULL) {
      printf("Error opening directory\n");
      exit(EXIT_FAILURE);
    }

    int x;
    for (x = 0; x < sizeof(positions) / sizeof(positions[0]); x++) {
        char dir_name[MAX_FILENAME];
        sprintf(dir_name, "./%s", positions[x]);

        if (access(dir_name, F_OK) != 0) {
            char *args[] = {
                "mkdir",
                dir_name,
                NULL
            };
            pid_t pid = fork();

            if (pid == -1) {
                perror("fork");
                exit(EXIT_FAILURE);
            } else if (pid == 0) {
                execvp(args[0], args);
                perror("execvp");
                exit(EXIT_FAILURE);
            } else {
                int status;
                waitpid(pid, &status, 0);
                if (WIFEXITED(status) && WEXITSTATUS(status) != 0) {
                    printf("Error creating directory: %s\n", dir_name);
                    exit(EXIT_FAILURE);
                }
            }
        }
    }

    rewinddir(dir);

    while ((entry = readdir(dir)) != NULL) {
      if (entry -> d_type == DT_REG) { // jika entri adalah file biasa
        image_file = entry -> d_name;
        char * ext = strrchr(image_file, '.');

        if (ext != NULL && (strcmp(ext, ".jpeg") == 0 || strcmp(ext, ".jpg") == 0 || strcmp(ext, ".png") == 0)) {
          int x;
          for (x = 0; x < sizeof(positions) / sizeof(positions[0]); x++) {
            if (strstr(image_file, positions[x]) != NULL) {
              char new_path[MAX_FILENAME];
              sprintf(new_path, "./%s/%s", positions[x], image_file);

              char old_path[MAX_FILENAME];
              sprintf(old_path, "./players/%s", image_file);

              char * args[] = {
                "mv",
                old_path,
                new_path,
                NULL
              };
              pid_t pid = fork();

              if (pid == -1) {
                perror("fork");
                exit(EXIT_FAILURE);
              } else if (pid == 0) {
                execvp(args[0], args);
                perror("execvp");
                exit(EXIT_FAILURE);
              } else {
                int status;
                waitpid(pid, & status, 0);
                if (WIFEXITED(status) && WEXITSTATUS(status) != 0) {
                  printf("Error moving file: %s\n", image_file);
                  exit(EXIT_FAILURE);
                }
              }
              break;
            }
          }
        }
      }
    }

    closedir(dir);

    char * args[] = {
      "rm",
      "-r",
      "./players",
      NULL
    };
    pid_t pid = fork();

    if (pid == -1) {
      perror("fork");
      exit(EXIT_FAILURE);
    } else if (pid == 0) {
      execvp(args[0], args);
      perror("execvp");
      exit(EXIT_FAILURE);
    } else {
      int status;
      waitpid(pid, & status, 0);
      if (WIFEXITED(status) && WEXITSTATUS(status) != 0) {
        printf("Error removing directory: %s\n", "./players");
        exit(EXIT_FAILURE);
      }
    }
}
void make_team(int bek, int gelandang, int striker) {
  if (bek + gelandang + striker > 10) {
    printf("Formasi tidak bisa dibuat, Pemain terlalu banyak\n");
    return;
  }
  char * positions[] = {
    "Kiper",
    "Bek",
    "Gelandang",
    "Penyerang"
  };

  char * formasi = malloc(sizeof(char) * MAX_FILENAME);
  sprintf(formasi, "/home/testing/Formasi_%d-%d-%d.txt", bek, gelandang, striker);
  FILE * teamFile = fopen(formasi, "w");
  if (teamFile == NULL) {
    printf("Error creating team file: %s\n", strerror(errno));
    return;
  }
  fprintf(teamFile, "Formasi %d-%d-%d\n\n", bek, gelandang, striker);

  int ratings[5][20] = {
    0
  }; // Array untuk menyimpan rating semua pemain
  char playerNames[5][20][MAX_FILENAME]; // Array untuk menyimpan nama semua pemain
  int count[5] = {
    0
  }; // Array untuk menyimpan jumlah pemain di setiap posisi

  // Buka setiap folder posisi dan dapatkan rating dan nama setiap pemain
  for (int x = 0; x < 4; x++) {
    DIR * dir = opendir(positions[x]);
    if (dir == NULL) {
      printf("Error opening directory %s\n", positions[x]);
      continue;
    }
    struct dirent * entry;
    char fileName[MAX_FILENAME];
    while ((entry = readdir(dir)) != NULL) {
      if (entry -> d_type == DT_REG) { // jika entri adalah file biasa
        sprintf(fileName, "%s/%s", positions[x], entry -> d_name);
        char * rating = strrchr(entry -> d_name, '_') + 1; // dapatkan rating dari nama file
        char * name = entry -> d_name;
        name[strlen(name) - strlen(rating) - 1] = '\0'; // hapus peringkat dari nama file
        int ratingVal = atoi(rating);
        ratings[x + 1][count[x + 1]] = ratingVal;
        strcpy(playerNames[x + 1][count[x + 1]], name); // copy nama pemain ke array
        count[x + 1]++;
      }
    }
    closedir(dir);
  }

  // Dapatkan pemain terbaik untuk setiap posisi berdasarkan rating mereka
  int bestKiper = 0;
  int bestBek[bek];
  int bestGelandang[gelandang];
  int bestPenyerang[striker];

  for (int x = 0; x < count[1]; x++) {
    if (ratings[1][x] > ratings[1][bestKiper]) {
      bestKiper = x;
    }
  }

  for (int x = 0; x < bek; x++) {
    bestBek[x] = -1;
  }

  for (int x = 0; x < count[2]; x++) {
    for (int y = 0; y < bek; y++) {
      if (bestBek[y] == -1 || ratings[2][x] > ratings[2][bestBek[y]]) {
        int z;
        for (z = bek - 1; z > y; z--) {
          bestBek[z] = bestBek[z - 1];
        }
        bestBek[y] = x;
        break;
      }
    }
  }

  for (int x = 0; x < gelandang; x++) {
    bestGelandang[x] = -1;
  }

  for (int x = 0; x < count[3]; x++) {
    for (int y = 0; y < gelandang; y++) {
      if (bestGelandang[y] == -1 || ratings[3][x] > ratings[3][bestGelandang[y]]) {
        int z;
        for (z = gelandang - 1; z > y; z--) {
          bestGelandang[z] = bestGelandang[z - 1];
        }
        bestGelandang[y] = x;
        break;
      }
    }
  }

  for (int x = 0; x < striker; x++) {
    bestPenyerang[x] = -1;
  }

  for (int x = 0; x < count[4]; x++) {
    for (int y = 0; y < striker; y++) {
      if (bestPenyerang[y] == -1 || ratings[4][x] > ratings[4][bestPenyerang[y]]) {
        int z;
        for (z = striker - 1; z > y; z--) {
          bestPenyerang[z] = bestPenyerang[z - 1];
        }
        bestPenyerang[y] = x;
        break;
      }
    }
  }

  // Print pemain terbaik untuk setiap posisi ke file tim
  fprintf(teamFile, "Kiper: %s (%d)\n", playerNames[1][bestKiper], ratings[1][bestKiper]);
  fprintf(teamFile, "Bek:\n");
  for (int x = 0; x < bek; x++) {
    fprintf(teamFile, "- %s (%d)\n", playerNames[2][bestBek[x]], ratings[2][bestBek[x]]);
  }
  fprintf(teamFile, "Gelandang:\n");
  for (int x = 0; x < gelandang; x++) {
    fprintf(teamFile, "- %s (%d)\n", playerNames[3][bestGelandang[x]], ratings[3][bestGelandang[x]]);
  }
  fprintf(teamFile, "Penyerang:\n");
  for (int x = 0; x < striker; x++) {
    fprintf(teamFile, "- %s (%d)\n", playerNames[4][bestPenyerang[x]], ratings[4][bestPenyerang[x]]);
  }

  fclose(teamFile);
  free(formasi);
}
