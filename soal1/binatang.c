#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <time.h>
#include <dirent.h>
#include <sys/stat.h>

// inisialiasi nama folder
#define AIR_FOLDER "HewanAir/"
#define DARAT_FOLDER "HewanDarat/"
#define AMPHIBI_FOLDER "HewanAmphibi/"
// ekstensi file gambar
#define FILE_EXTENSION ".jpg" 

int main() {
    // deklarasi PID dan child process
    pid_t child_id1, child_id2, child_id3, child_id4, child_id5;

    int status;
    char* url = "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq";
    char* filezip = "binatang.zip";
    char* foldername = "unzip_binatang";

    // mendownload file dari URL
    char wget_command[1000];
    sprintf(wget_command, "wget -O %s '%s'", filezip, url);
    system(wget_command);

    child_id1 = fork();
    if(child_id1 < 0) exit(EXIT_FAILURE);
    if(child_id1 == 0){
        //mengekstrak zip 
        char *argv[] = {"unzip", "-oq", "/home/kali/Documents/Shift2/binatang.zip", "-d", "/home/kali/Documents/Shift2/unzip_binatang", NULL};
        execv("/usr/bin/unzip", argv);
    } else while((wait(&status)) > 0);

    // inisialisasi fungsi rand() untuk mengambil file gambar secara acak
    srand(time(NULL)); 

    DIR *dir;
    struct dirent *ent;
    char *filename;
    int file_count = 0;
    struct stat filestat;
    char path[256], air_path[256],amphibi_path[256], darat_path[256];

    // membuka direktori unzip_binatang
    dir = opendir(foldername);
    if (dir != NULL)
    {
        // menghitung jumlah file dalam direktori
        while ((ent = readdir(dir)) != NULL)
        {
            if (strstr(ent->d_name, FILE_EXTENSION) != NULL)
            {
                file_count++;
            }
        }

        // menutup direktori
        closedir(dir);

        // mengacak nomor file yang akan dipilih
        int file_number = rand() % file_count;

        // membuka direktori unzip_binatang lagi
        dir = opendir(foldername);
        if (dir != NULL)
        {
            // mencari file dengan nomor yang dipilih
            while ((ent = readdir(dir)) != NULL && file_number >= 0)
            {
                if (strstr(ent->d_name, FILE_EXTENSION) != NULL)
                {
                    if (file_number == 0)
                    {
                        // mengambil file yang dipilih secara acak
                        filename = malloc(strlen(foldername) + strlen(ent->d_name) + 2);
                        sprintf(filename, "%s/%s", foldername, ent->d_name);
                        break;
                    }
                    else
                    {
                        file_number--;
                    }
                }
            }

            // menutup direktori
            closedir(dir);

            // menampilkan nama file yang dipilih secara acak
            printf("File yang dipilih secara acak pada penjagaan malam ini : %s\n", filename);

            // membebaskan memori yang dialokasikan untuk nama file
            free(filename);
        }
        else
        {
            printf("Error: tidak dapat membuka direktori %s\n", foldername);
        }
    }
    else
    {
        printf("Error: tidak dapat membuka direktori %s\n", foldername);
    }

    // buat direktori HewanDarat, HewanAmphibi, dan HewanAir   
    system("mkdir -p HewanDarat");
    printf("Folder HewanDarat berhasil dibuat.\n");

    system("mkdir -p HewanAmphibi");
    printf("Folder HewanAmphibi berhasil dibuat.\n");

    system("mkdir -p HewanAir");
    printf("Folder HewanAir berhasil dibuat.\n");



    // membuka direktori "unzip_binatang"
    dir = opendir("unzip_binatang");
    if (dir == NULL) {
        perror("Gagal membuka direktori");
        exit(EXIT_FAILURE);
    }

    // iterasi setiap file dalam direktori "unzip_binatang"
    while ((ent = readdir(dir)) != NULL) {
        // mengabaikan file yang berupa direktori atau file tersembunyi
        if (ent->d_type == DT_DIR || ent->d_name[0] == '.') {
            continue;
        }
        
        // membuat path lengkap untuk file yang sedang diproses
        sprintf(path, "unzip_binatang/%s", ent->d_name);
        
        // memeriksa untuk hewan air
        if (strstr(ent->d_name, "air") != NULL) {
            // Membuat jalur file baru untuk folder "air"
            sprintf(air_path, AIR_FOLDER "%s", ent->d_name);
            // Memindahkan file ke folder "air"
            if (rename(path, air_path) != 0) {
                perror("Gagal memindahkan file air");
            }
        // memeriksa untuk hewan amphibi
        } else if (strstr(ent->d_name, "amphibi") != NULL) {
            // Membuat jalur file baru untuk folder "amphibi"
            sprintf(amphibi_path, AMPHIBI_FOLDER "%s", ent->d_name);
            // Memindahkan file ke folder "amphibi"
            if (rename(path, amphibi_path) != 0) {
                perror("Gagal memindahkan file amphibi");
            }
        // memeriksa untuk hewan darat
        } else if (strstr(ent->d_name, "darat") != NULL) {
            // Membuat jalur file baru untuk folder "darat"
            sprintf(darat_path, DARAT_FOLDER "%s", ent->d_name);
            // Memindahkan file ke folder "darat"
            if (rename(path, darat_path) != 0) {
                perror("Gagal memindahkan file darat");
            }
        }
    }

    // menutup direktori
    closedir(dir);
    
    child_id2 = fork();
    if(child_id2 < 0) {
        exit(EXIT_FAILURE);
    }
   
    if(child_id2 == 0){
        //melakukan zip 
        char *argv[] = {"zip", "-r", "HewanAir.zip", "HewanAir", NULL};
        execv("/usr/bin/zip", argv);
    } else while((wait(&status)) > 0);
    
    child_id3 = fork();
    if(child_id3 < 0) {
        exit(EXIT_FAILURE);
    }
   
    if(child_id3 == 0){
        //melakukan zip 
        char *argv[] = {"zip", "-r", "HewanDarat.zip", "HewanDarat", NULL};
        execv("/usr/bin/zip", argv);
    } else while((wait(&status)) > 0);
    
    child_id4 = fork();
    if(child_id4 < 0) {
        exit(EXIT_FAILURE);
    }
   
    if(child_id4 == 0){        
        //melakukan zip 
        char *argv[] = {"zip", "-r", "HewanAmphibi.zip", "HewanAmphibi", NULL};
        execv("/usr/bin/zip", argv);
    } else while((wait(&status)) > 0);
    
    // menghapus direktori yang tidak diperlukan, seperti HewanDarat, HewanAmphibi, dan HewanAir, unzip_binatang
    system("rm -r HewanDarat");
    printf("Folder HewanDarat berhasil dihapus guna untuk menghemat memori\n");
    
    system("rm -r HewanAmphibi");
    printf("Folder HewanAmphibi berhasil dihapus guna untuk menghemat memori\n");
    
    system("rm -r HewanAir");
    printf("Folder HewanAir berhasil dihapus guna untuk menghemat memori\n");

    system("rm -r unzip_binatang");
    printf("Folder Unzip_Binatang berhasil dihapus guna untuk menghemat memori\n");
        
    return 0;
}
