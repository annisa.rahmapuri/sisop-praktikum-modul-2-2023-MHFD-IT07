#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <dirent.h>
#include <curl/curl.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <syslog.h>
#include <fcntl.h>

#define MAX_FILENAME_LEN 100

void daemonize() {
    pid_t pid;

    pid = fork();

    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    if (setsid() < 0) {
        exit(EXIT_FAILURE);
    }

    umask(0);

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    openlog("my_daemon", LOG_PID, LOG_DAEMON);
}

// fungsi untuk melakukan download gambar
int download_image(char *url, char *filename) {
    CURL *curl;
    CURLcode res;
    FILE *fp;

    curl = curl_easy_init();
    if (curl) {
        fp = fopen(filename, "wb");
        curl_easy_setopt(curl, CURLOPT_URL, url);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
        res = curl_easy_perform(curl);
        curl_easy_cleanup(curl);
        fclose(fp);
        return 0;
    } else {
        return 1;
    }
}

void create_killer(char mode, char *namaProgram) {
    FILE *fp;
    fp = fopen("killer.sh", "w");
    if (mode == 'a') {
        fprintf(fp, "#!/bin/bash\n");
        fprintf(fp, "pkill %s\n", namaProgram);
    } else if (mode == 'b') {
        fprintf(fp, "#!/bin/bash\n");
        fprintf(fp, "kill -SIGTERM %d\n", getpid() + 2);
    }
    fprintf(fp, "rm killer.sh\n"); 
    fclose(fp);
    pid_t pid = fork();
    if (pid == 0){
        execl("/bin/chmod", "chmod", "+x", "killer.sh", NULL);
    }
}

int main(int argc, char *argv[]) {
    pid_t pid, sid;

    if (argc < 2) {
        printf("Usage: program a/b\n");
        return 0;
    }

    char *namaProgram = argv[0];
    char mode = *argv[1];
    create_killer(mode, namaProgram);

    time_t t;
    struct tm *tm;
    char timestamp[20], filename[MAX_FILENAME_LEN];
    int i, width, height;
    DIR *dir;
    struct dirent *entry;
    struct stat file_stat;
    int count;

    daemonize();

    chdir("/home/asxklm/sisop/modul 2");

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while (1) {
    // mendapatkan timestamp saat ini
    t = time(NULL);
    tm = localtime(&t);
    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d_%H:%M:%S", tm);

    // membuat folder dengan nama timestamp
    mkdir(timestamp, 0777);
    chdir(timestamp);

    // melakukan download 15 gambar
    for (i = 0; i < 15; i++) {
        // mendapatkan ukuran gambar
        width = (t % 1000) + 50;
        height = width;

        // membuat nama file gambar dengan nomor iterasi
        snprintf(filename, MAX_FILENAME_LEN, "%s-%d.jpg", timestamp, i);
        download_image("https://picsum.photos/", filename);
        sleep(5);
    }

    // meng-zip folder
    dir = opendir(".");
    count = 0;
    while ((entry = readdir(dir))) {
        if (entry->d_type == DT_REG) {
            count++;
        }
    }
    if (count == 15) {
        chdir("..");
        char zip_command[50];
        sprintf(zip_command, "zip -r %s.zip %s", timestamp, timestamp);
        system(zip_command);
        chdir(timestamp);
        for (i = 0; i < 15; i++) {
            snprintf(filename, MAX_FILENAME_LEN, "%s-%d.jpg", timestamp, i);
            remove(filename);
        }
        chdir("..");
        rmdir(timestamp);
    }
}
}
