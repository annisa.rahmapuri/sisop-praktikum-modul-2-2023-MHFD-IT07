# Laporan Praktikum Sistem Operasi - Modul 2

## Soal 1

Grape-kun adalah seorang penjaga hewan di kebun binatang, dia mendapatkan tugas dari atasannya untuk melakukan penjagaan pada beberapa hewan-hewan yang ada di kebun binatang sebelum melakukan penjagaan Grape-kun harus mengetahui terlebih dahulu hewan apa aja yang harus dijaga dalam drive kebun binatang tersebut terdapat folder gambar dari hewan apa saja yang harus dijaga oleh Grape-kun. Berikut merupakan link download dari drive kebun binatang tersebut : [https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq](https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq)

1. Grape-kun harus mendownload file tersebut untuk disimpan pada penyimpanan local komputernya. Dan untuk melakukan melihat file gambar pada folder yang telah didownload Grape-kun harus melakukan unzip pada folder tersebut.
2. Setelah berhasil melakukan unzip Grape-kun melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift penjagaan pada hewan tersebut.
3. Karena Grape-kun adalah orang yang perfeksionis Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. Direktori tersebut dengan nama **HewanDarat, HewanAmphibi,** dan **HewanAir.** Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya.
4. Setelah mengetahui hewan apa saja yang harus dijaga Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan.

Catatan :

- untuk melakukan zip dan unzip tidak boleh menggunakan system

### **1.1**

Kami diminta untuk mendownload file yang disimpan di URL [https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq](https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq). Setelah berhasil, akan tersimpan file dengan format zip dan kami diminta untuk unzip folder binatang tersebut. Berikut ialah langkah pengerjaannya :

- Import beberapa library dan pendefinisian beberapa konstanta string.
    
    ```c
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include <unistd.h>
    #include <sys/wait.h>
    #include <time.h>
    #include <dirent.h>
    #include <sys/stat.h>
    
    // inisialiasi nama folder
    #define AIR_FOLDER "HewanAir/"
    #define DARAT_FOLDER "HewanDarat/"
    #define AMPHIBI_FOLDER "HewanAmphibi/"
    // ekstensi file gambar
    #define FILE_EXTENSION ".jpg" 
    ```
    
    Penjelasan :
    
    - Import beberapa library yang dibutuhkan dengan sintaks `#include` serta melakukan pendefinisian beberapa konstanta string untuk nama folder dan ekstensi file gambar dengan sistaks `#define`
- Proses Download dan Unzip
    
    ```c
    int main() {
        // deklarasi PID dan child process
        pid_t child_id1, child_id2, child_id3, child_id4, child_id5;
    		int status;
    
        char* url = "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq";
        char* filezip = "binatang.zip";
        char* foldername = "unzip_binatang";
    ```
    
    - Pada bagian ini, program mendeklarasikan beberapa variabel yang akan digunakan di dalam program. Variabel `child_id1`, `child_id2`, `child_id3`, `child_id4`, dan `child_id5` adalah tipe data `pid_t`, yang akan menampung ID proses child yang akan dibuat nanti. Variabel `status`merepresentasikan status keluaran dari proses child.
    - Selain itu, terdapat juga variabel-string `url`, `filezip`, dan `foldername` yang menyimpan alamat URL untuk file yang ingin didownload, nama file zip yang akan disimpan, dan nama folder tempat hasil ekstraksi akan diletakkan.
    
    ```c
    // mendownload file dari URL
        char wget_command[1000];
        sprintf(wget_command, "wget -O %s '%s'", filezip, url);
        system(wget_command);
    ```
    
    - Program kemudian membuat perintah `wget`dengan menggabungkan variabel `filezip` dan `url` menjadi satu string, lalu menjalankan perintah tersebut dengan fungsi `system()`. Ini akan mendownload file zip dari `url` dan menyimpannya dengan nama `filezip`.
    
    ```c
        child_id1 = fork();
        if(child_id1 < 0) exit(EXIT_FAILURE);
        if(child_id1 == 0){
            //mengekstrak zip 
            char *argv[] = {"unzip", "-oq", "/home/kali/Documents/Shift2/binatang.zip", "-d", "/home/kali/Documents/Shift2/unzip_binatang", NULL};
            execv("/usr/bin/unzip", argv);
        } else while((wait(&status)) > 0);
    ```
    
    - Pada bagian ini, program membuat proses child pertama dengan menggunakan fungsi `fork()`. Jika `fork()` mengembalikan nilai negatif, maka program akan keluar dengan status kesalahan (`exit(EXIT_FAILURE)`).
    - Jika `fork()` mengembalikan nilai 0, maka itu berarti program sedang berjalan di dalam proses child. Selanjutnya, program akan mengekstrak isi dari file zip yang sebelumnya didownload ke dalam sebuah folder dengan menggunakan perintah `unzip` yang dijalankan menggunakan fungsi `execv()`. Variabel string `argv` memuat argumen untuk menjalankan perintah tersebut. Setelah proses child selesai, program akan menunggu hingga proses tersebut selesai menggunakan fungsi `wait(&status)`.

### **1.2**

Kami diminta untuk melakukan shift penjagaan pada hewan tersebut dengan cara melakukan pemilihan secara acak pada file gambar. Berikut ialah langkah pengerjaannya :

- Proses Shift Penjangaan
    
    ```c
    srand(time(NULL));
    ```
    
    - Fungsi `srand()` digunakan untuk menginisialisasi generator bilangan acak. `time()` digunakan agar seed (benih) yang dihasilkan selalu berbeda setiap kali program dijalankan.
    
    ```c
    DIR *dir;
    struct dirent *ent;
    char *filename;
    int file_count = 0;
    struct stat filestat;
    char path[256], air_path[256],amphibi_path[256], darat_path[256];
    ```
    
    - Mendeklarasikan variabel yang akan digunakan dalam program, seperti pointer ke direktori (`dir`), pointer ke struktur dirent (`ent`), nama file (`filename`), jumlah file dalam direktori (`file_count`), serta path yang digunakan untuk filtering folder nanti.
    
    ```c
    dir = opendir(foldername);
    if (dir != NULL)
    {
        while ((ent = readdir(dir)) != NULL)
        {
            if (strstr(ent->d_name, FILE_EXTENSION) != NULL)
            {
                file_count++;
            }
        }
        closedir(dir);
    ```
    
    - Pada baian ini akan membuka direktori dengan fungsi `opendir()`, kemudian menggunakan perulangan `while` untuk membaca isi direktori dengan fungsi `readdir()`. Jika ditemukan file yang memiliki ekstensi tertentu (`FILE_EXTENSION`), maka `file_count` akan ditambah 1.
    
    ```c
    int file_number = rand() % file_count;
    ```
    
    - Menghasilkan nomor file secara acak antara 0 hingga jumlah file dalam direktori ditambah 1.
    
    ```c
    dir = opendir(foldername);
    if (dir != NULL)
    {
        while ((ent = readdir(dir)) != NULL && file_number >= 0)
        {
            if (strstr(ent->d_name, FILE_EXTENSION) != NULL)
            {
                if (file_number == 0)
                {
                    filename = malloc(strlen(foldername) + strlen(ent->d_name) + 2);
                    sprintf(filename, "%s/%s", foldername, ent->d_name);
                    break;
                }
                else
                {
                    file_number--;
                }
            }
        }
        closedir(dir);
    
        printf("File yang dipilih secara acak pada penjagaan malam ini : %s\n", filename);
        free(filename);
    }
    
    ```
    
    - Membuka kembali direktori dengan fungsi `opendir()`, kemudian menggunakan perulangan `while` untuk mencari file dengan nomor yang telah dihasilkan sebelumnya. Jika ditemukan file dengan nomor tersebut, maka nama file akan disimpan di variabel `filename` dan program akan keluar dari perulangan `while`. Kemudian, nama file akan ditampilkan ke layar menggunakan fungsi `printf()`  "File yang dipilih secara acak pada penjagaan malam ini : `filename`"

### **1.3**

Kami diminta untuk membuat direktori baru (**HewanDarat, HewanAmphibi,** dan **HewanAir)** dan memilah file gambar tersebut berdasarkan tempat tinggalnya sesuai dengan format nama masing masing file. Berikut ialah langkah pengerjaannya :

- Proses Filtering Berdasarkan Nama File
    
    ```c
     system("mkdir -p HewanDarat");
     printf("Folder HewanDarat berhasil dibuat.\n");
    
     system("mkdir -p HewanAmphibi");
     printf("Folder HewanAmphibi berhasil dibuat.\n");
    
     system("mkdir -p HewanAir");
     printf("Folder HewanAir berhasil dibuat.\n");
    ```
    
    - Bagian ini memanfaatkan fungsi `system()` untuk menjalankan perintah `mkdir` pada sistem operasi yang digunakan. Perintah `mkdir -p <nama folder>` digunakan untuk membuat folder baru, dengan opsi `-p` yang menentukan apakah direktori induk harus dibuat jika belum ada. Setelah setiap folder dibuat, program akan menampilkan pesan bahwa folder telah berhasil dibuat.
    
    ```c
     dir = opendir("unzip_binatang");
     if (dir == NULL) {
         perror("Gagal membuka direktori");
         exit(EXIT_FAILURE);
     }
    
     while ((ent = readdir(dir)) != NULL) {
         // ...
     }
    
     closedir(dir);
    ```
    
    - Bagian ini menggunakan fungsi `opendir()` untuk membuka folder "unzip_binatang". Kemudian, program melakukan pengecekan apakah folder telah berhasil dibuka atau tidak. Jika gagal, program akan menampilkan pesan kesalahan dan keluar dari program. Selanjutnya, program melakukan iterasi menggunakan fungsi `readdir()` untuk setiap file di dalam folder "unzip_binatang". Pengecekan ini akan dilakukan pada bagian selanjutnya.
    
    ```c
    
    // memeriksa untuk hewan air
            if (strstr(ent->d_name, "air") != NULL) {
                // Membuat jalur file baru untuk folder "air"
                sprintf(air_path, AIR_FOLDER "%s", ent->d_name);
                // Memindahkan file ke folder "air"
                if (rename(path, air_path) != 0) {
                    perror("Gagal memindahkan file air");
                }
            // memeriksa untuk hewan amphibi
            } else if (strstr(ent->d_name, "amphibi") != NULL) {
                // Membuat jalur file baru untuk folder "amphibi"
                sprintf(amphibi_path, AMPHIBI_FOLDER "%s", ent->d_name);
                // Memindahkan file ke folder "amphibi"
                if (rename(path, amphibi_path) != 0) {
                    perror("Gagal memindahkan file amphibi");
                }
            // memeriksa untuk hewan darat
            } else if (strstr(ent->d_name, "darat") != NULL) {
                // Membuat jalur file baru untuk folder "darat"
                sprintf(darat_path, DARAT_FOLDER "%s", ent->d_name);
                // Memindahkan file ke folder "darat"
                if (rename(path, darat_path) != 0) {
                    perror("Gagal memindahkan file darat");
                }
            }
    ```
    
    - Selanjutnya, program akan memeriksa apakah nama file mengandung kata kunci "air", "amphibi", atau "darat" menggunakan fungsi `strstr()`. Jika iya, program akan memindahkan file tersebut ke folder yang sesuai menggunakan perintah `rename()` pada sistem operasi yang digunakan.
        - Sebagai contoh, program akan melakukan pengecekan menggunakan fungsi `strstr()` apakah nama file mengandung kata kunci "air". Jika iya, program membentuk path lengkap untuk folder tujuan dengan memanfaatkan variabel `AIR_FOLDER`, dan nama file yang sedang diproses. Kemudian, program memindahkan file tersebut ke folder "HewanAir" menggunakan perintah `rename()`.
    
    ```c
     closedir(dir);
    ```
    
    - Terakhir, program menutup folder "unzip_binatang" yang telah dibuka pada awal eksekusi menggunakan fungsi `closedir()`.

### **1.4**

Kami diminta untuk melakukan zip kepada direktori dibuat sebelumnya agar menghemat penyimpanan. Kami juga diminta untuk menghapus folder HewanDarat, HewanAmphibi, dan HewanAir serta folder kosong Unzip_Binantang. Berikut ialah langkah pengerjaannya :

- Proses Hapus File dan Zip
    
    ```c
    // membuat child process dengan memanggil sistem call fork()
    child_id2 = fork();
    if(child_id2 < 0) {
        exit(EXIT_FAILURE);
    }
    if(child_id2 == 0){
    //melakukan zip
        char *argv[] = {"zip", "-r", "HewanAir.zip", "HewanAir", NULL};
        execv("/usr/bin/zip", argv);
    } else while((wait(&status)) > 0);
    
    child_id3 = fork();
    if(child_id3 < 0) {
        exit(EXIT_FAILURE);
    }
    if(child_id3 == 0){
        //melakukan zip 
        char *argv[] = {"zip", "-r", "HewanDarat.zip", "HewanDarat", NULL};
        execv("/usr/bin/zip", argv);
    } else while((wait(&status)) > 0);
    
    child_id4 = fork();
    if(child_id4 < 0) {
        exit(EXIT_FAILURE);
    }
    if(child_id4 == 0){        
        //melakukan zip 
        char *argv[] = {"zip", "-r", "HewanAmphibi.zip", "HewanAmphibi", NULL};
        execv("/usr/bin/zip", argv);
    } else while((wait(&status)) > 0);
    ```
    
    - Pada bagian ini, program membuat child process baru dan menjalankan operasi `zip` direktori HewanAir ke dalam berkas bernama HewanAir.zip. Child process ini dihasilkan oleh pemanggilan sistem `fork()`. Jika fork() berhasil, maka child process akan mengeksekusi perintah zip menggunakan fungsi `execv()`. Setelah selesai dieksekusi, parent process menunggu hingga child process selesai dengan memanggil `wait(&status)`
    - Proses ini berulan juga untuk folder HewanDarat dan HewanAmphibi
    
    ```c
    system("rm -r HewanDarat");
    printf("Folder HewanDarat berhasil dihapus guna untuk menghemat memori\n");
    
    system("rm -r HewanAmphibi");
    printf("Folder HewanAmphibi berhasil dihapus guna untuk menghemat memori\n");
    
    system("rm -r HewanAir");
    printf("Folder HewanAir berhasil dihapus guna untuk menghemat memori\n");
    
    system("rm -r unzip_binatang");
    printf("Folder Unzip_Binatang berhasil dihapus guna untuk menghemat memori\n");
    ```
    
    - Bagian ini bertanggung jawab untuk menghapus direktori yang telah di-zip agar tidak memakan banyak ruang penyimpanan komputer. Program menggunakan fungsi `system()` untuk menjalankan perintah shell `rm - r <nama_direktori>` untuk menghapus setiap direktori yang telah di-zip. Kemudian mencetak sebuah pesan untuk memberi tahu bahwa folder tersebut telah berhasil dihapus.

## Output Soal 1

### **Terminal**
![Alt Text](https://i.ibb.co/n6MfKcR/Screenshot-2023-04-08-184446.png)

### **File Explorer**
![Alt Text](https://i.ibb.co/P1tCvYC/Screenshot-2023-04-08-184527.png)


## Soal 2

Sucipto adalah seorang seniman terkenal yang berasal dari Indonesia. Karya nya sudah terkenal di seluruh dunia, dan lukisannya sudah dipajang di berbagai museum mancanegara. Tetapi, akhir-akhir ini sucipto sedang terkendala mengenai ide lukisan ia selanjutnya. Sebagai teman yang jago sisop, bantu sucipto untuk melukis dengan mencarikannya gambar-gambar di internet sebagai referensi !

1. Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].
2. Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari [https://picsum.photos/](https://picsum.photos/), dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].
3. Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip).
4. Karena takut program tersebut lepas kendali, Sucipto ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.
5. Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).

Catatan :

- Tidak boleh menggunakan system()
- Proses berjalan secara daemon
- Proses download gambar pada beberapa folder dapat berjalan secara bersamaan (overlapping)

### **2.1 & 2.2**

Kami diminta untuk buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss]. Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari [https://picsum.photos/](https://picsum.photos/), dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].Berikut ialah langkah pengerjaannya :

- Import beberapa library dan pendefinisian beberapa konstanta string.
    
    ```c
    #include <stdio.h>
    #include <stdlib.h>
    #include <unistd.h>
    #include <time.h>
    #include <dirent.h>
    #include <curl/curl.h>
    #include <string.h>
    #include <sys/stat.h>
    #include <sys/types.h>
    #include <syslog.h>
    #include <fcntl.h>

    #define MAX_FILENAME_LEN 100 
    ```
    
    Penjelasan :
    
    - Import beberapa library yang dibutuhkan dengan sintaks `#include` serta melakukan pendefinisian beberapa konstanta curl untuk mendownload dan ekstensi file gambar dengan sistaks `#define`

- Proses Download 
    
    ```c
    int download_image(char *url, char *filename) {
    CURL *curl;
    CURLcode res;
    FILE *fp;

    curl = curl_easy_init();
    if (curl) {
        fp = fopen(filename, "wb");
        curl_easy_setopt(curl, CURLOPT_URL, url);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
        res = curl_easy_perform(curl);
        curl_easy_cleanup(curl);
        fclose(fp);
        return 0;
        } else {
        return 1;
        }
    }
    ```
    Penjelasan :
    
    - Pada potongan kode tersebut, pertama-tama `curl_easy_init()` digunakan untuk memulai sesi curl. Fungsi ini mengembalikan pointer `CURL *` ke objek `CURL` yang baru dibuat atau `NULL` jika terjadi kesalahan.
    
    - Selanjutnya, dijalankan pengecekan `if (curl)` untuk memeriksa apakah objek `CURL` telah berhasil dibuat dengan benar atau tidak. Jika objek telah berhasil dibuat, maka program membuka file untuk ditulis dengan menggunakan fungsi `fopen(filename, "wb")` dan menyimpan hasilnya ke dalam pointer `FILE *fp`. Fungsi `fopen()` digunakan untuk membuka file dengan mode `"wb"` (write binary) yang berarti bahwa file akan ditulis dalam mode biner.
    
    - Setelah itu, program mengatur beberapa opsi curl yang dibutuhkan dengan menggunakan `curl_easy_setopt()`. Dalam potongan kode tersebut, program mengatur `CURLOPT_URL` sebagai URL yang akan diunduh dan `CURLOPT_WRITEDATA` sebagai pointer ke file yang akan ditulis.
    
    - Selanjutnya, program menjalankan proses pengunduhan file dengan menggunakan `curl_easy_perform()` dan menyimpan hasilnya ke dalam variabel res. Setelah proses pengunduhan selesai, program membersihkan sesi CURL dengan menggunakan `curl_easy_cleanup(curl)` dan menutup file dengan menggunakan `fclose(fp)`.
    
    - Jika objek `CURL` tidak berhasil dibuat atau terjadi kesalahan selama proses pengunduhan, maka program akan mengembalikan nilai `1` sebagai tanda terjadinya kesalahan. Jika tidak terjadi kesalahan, program akan mengembalikan nilai `0` sebagai tanda bahwa pengunduhan telah berhasil dilakukan.

- Pengambilan Timestamp
    
    ```c
    time_t t;
    struct tm *tm;
    char timestamp[20], filename[MAX_FILENAME_LEN];
    int i, width, height;
    DIR *dir;
    struct dirent *entry;
    struct stat file_stat;
    int count;
    ```
    Penjelasan:

    time_t t;: Membuat variabel t bertipe data time_t, yang akan digunakan untuk menyimpan nilai timestamp (waktu) saat ini.

    - `struct tm *tm`: Membuat pointer ke struktur tm bertipe data struct tm, yang akan digunakan untuk menyimpan informasi waktu seperti tahun, bulan, hari, jam, menit, dan detik.
    
    - `char timestamp[20]`, `filename[MAX_FILENAME_LEN]`: Membuat dua array karakter, yaitu timestamp dengan ukuran 20 karakter dan filename dengan ukuran maksimum MAX_FILENAME_LEN karakter, yang akan digunakan untuk menyimpan string timestamp dan nama file.
     
    - `int i`, `width`, `height`: Membuat tiga variabel bertipe data int, yaitu i, width, dan height, yang akan digunakan untuk looping dan menyimpan informasi ukuran gambar.
     
    - `DIR *dir`: Membuat pointer ke struktur dir bertipe data DIR, yang akan digunakan untuk merepresentasikan sebuah direktori dalam sistem operasi.
    
    - `struct dirent *entry`: Membuat pointer ke struktur entry bertipe data struct dirent, yang akan digunakan untuk menyimpan informasi tentang entri dalam sebuah direktori, seperti nama file atau nama direktori.
    
    - `struct stat file_stat`: Membuat variabel file_stat bertipe data struct stat, yang akan digunakan untuk menyimpan informasi tentang status (metadata) file, seperti ukuran file, waktu modifikasi, dan hak akses.
     
    - `int count`: Membuat variabel count bertipe data int, yang akan digunakan untuk menyimpan jumlah file atau entri dalam sebuah direktori.

- Melakukan Penamaan Folder dengan Timestamp
    
    ```c
    while (1) {
    // mendapatkan timestamp saat ini
    t = time(NULL);
    tm = localtime(&t);
    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d_%H:%M:%S", tm);

    // membuat folder dengan nama timestamp
    mkdir(timestamp, 0777);
    chdir(timestamp);
    ```
    Penjelasan:

    Baris kode di atas merupakan bagian dari suatu loop while yang berjalan secara terus menerus (infinite loop) dengan kondisi `while(1)`, yang akan berjalan selama program dijalankan.

    - `t = time(NULL)`: Menggunakan fungsi time() untuk mengambil nilai timestamp (waktu) saat ini dalam jumlah detik sejak epoch (1 Januari 1970), dan menyimpannya ke dalam variabel t bertipe data time_t.
     
    - `tm = localtime(&t)`: Menggunakan fungsi localtime() untuk mengkonversi nilai timestamp dalam variabel t menjadi struktur tm yang berisi informasi waktu seperti tahun, bulan, hari, jam, menit, dan detik dalam waktu lokal (berdasarkan waktu sistem operasi), dan menyimpannya ke dalam pointer tm bertipe data struct tm.
     
    - `strftime(timestamp, sizeof(timestamp), "%Y-%m-%d_%H:%M:%S", tm)`: Menggunakan fungsi strftime() untuk mengformat informasi waktu dalam struktur tm menjadi string dengan format tertentu, dan menyimpannya dalam variabel timestamp. Format yang digunakan adalah "%Y-%m-%d_%H:%M:%S", yang akan menghasilkan string berisi tahun (4 digit), bulan (2 digit), hari (2 digit), jam (2 digit), menit (2 digit), dan detik (2 digit), dipisahkan oleh tanda "-" dan ":". 
     
    - `mkdir(timestamp, 0777)`: Menggunakan fungsi mkdir() untuk membuat direktori dengan nama yang sama dengan nilai dalam variabel timestamp, dengan hak akses (permission) 0777. 
    
    - `chdir(timestamp)`: Menggunakan fungsi chdir() untuk mengubah direktori kerja (current working directory) ke direktori yang baru saja dibuat dengan nama yang sama dengan nilai dalam variabel timestamp. 

- Melakukan Download sebanyak 15 kali

    ```c
    // melakukan download 15 gambar
    for (i = 0; i < 15; i++) {
        // mendapatkan ukuran gambar
        width = (t % 1000) + 50;
        height = width;

        // membuat nama file gambar dengan nomor iterasi
        snprintf(filename, MAX_FILENAME_LEN, "%s-%d.jpg", timestamp, i);
        download_image("https://picsum.photos/", filename);
        sleep(5);
    }
    ```
    Penjelasan:

    Baris kode di atas merupakan bagian dari loop for yang berjalan 15 kali, dengan kondisi i < 15, yang bertujuan untuk melakukan download 15 gambar dari internet. 
     
    - `width = (t % 1000) + 50`: Menggunakan operasi modulo (%) untuk mengambil sisa hasil bagi dari nilai t dibagi dengan 1000, kemudian ditambahkan 50. 
    
    - `height = width`: Mengassign nilai variabel width ke variabel height, sehingga ukuran tinggi (height) gambar akan sama dengan ukuran lebar (width), sehingga gambar yang didownload akan berbentuk kotak.
     
    - `snprintf(filename, MAX_FILENAME_LEN, "%s-%d.jpg", timestamp, i)`: Menggunakan fungsi snprintf() untuk memformat nama file gambar yang akan didownload. Format yang digunakan adalah "%s-%d.jpg", dimana %s akan digantikan dengan nilai dalam variabel timestamp yang berisi timestamp saat ini, dan %d akan digantikan dengan nilai variabel i yang merupakan nomor iterasi saat ini. 
     
    - `download_image("https://picsum.photos/", filename)`: Memanggil fungsi download_image() dengan memberikan parameter URL "https://picsum.photos/" sebagai sumber gambar yang akan didownload, dan filename sebagai nama file yang akan disimpan.
     
    - `sleep(5)`: Menunda eksekusi program selama 5 detik menggunakan fungsi sleep() sebelum iterasi berikutnya dimulai. 


### **2.3**

Kami diminta setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip). Berikut ialah langkah pengerjaannya :

- Proses Melakukan ZIP dan menghapus folder bekas
    
    ```c
    // meng-zip folder
    dir = opendir(".");
    count = 0;
    while ((entry = readdir(dir))) {
        if (entry->d_type == DT_REG) {
            count++;
        }
    }
    if (count == 15) {
        chdir("..");
        char zip_command[50];
        sprintf(zip_command, "zip -r %s.zip %s", timestamp, timestamp);
        system(zip_command);
        chdir(timestamp);
        for (i = 0; i < 15; i++) {
            snprintf(filename, MAX_FILENAME_LEN, "%s-%d.jpg", timestamp, i);
            remove(filename);
        }
        chdir("..");
        rmdir(timestamp);
    }
    ```
    Penjelasan:
    
    Baris kode di atas merupakan bagian dari proses peng-zipan (compression) folder yang berisi gambar-gambar yang telah didownload sebelumnya.

    - `dir = opendir(".")`: Membuka folder saat ini (current directory) menggunakan fungsi opendir() dan menyimpan hasilnya dalam variabel dir.
    
    - `count = 0`: Menginisialisasi variabel count dengan nilai 0, yang akan digunakan untuk menghitung jumlah file yang ditemukan dalam folder saat ini.
     
    - `while ((entry = readdir(dir))) {`: Melakukan looping untuk membaca setiap entri (file/directory) dalam folder saat ini menggunakan fungsi readdir(). Looping ini akan berjalan selama masih ada entri yang belum dibaca.
     
    - `if (entry->d_type == DT_REG) {`: Memeriksa apakah entri yang sedang dibaca adalah sebuah file (bukan directory) dengan memeriksa nilai dari d_type dalam struktur entry.
     
    - `count++`: Menambahkan nilai variabel count setiap kali ditemukan sebuah file dalam folder saat ini.
     
    - `if (count == 15) {`: Memeriksa apakah jumlah file yang ditemukan dalam folder saat ini sama dengan 15, sesuai dengan jumlah gambar yang telah didownload sebelumnya.
     
    - `chdir("..")`: Pindah ke parent directory (directory di atasnya) menggunakan fungsi chdir() untuk keluar dari folder timestamp yang telah dibuat sebelumnya.
     
    - `char zip_command[50]`: Mendeklarasikan array karakter zip_command dengan panjang maksimum 50, yang akan digunakan untuk menyimpan perintah zip yang akan dijalankan.
     
    - `sprintf(zip_command, "zip -r %s.zip %s", timestamp, timestamp)`: Menggunakan fungsi sprintf() untuk memformat perintah zip yang akan dijalankan. Format yang digunakan adalah "zip -r %s.zip %s", dimana %s akan digantikan dengan nilai dalam variabel timestamp yang berisi nama folder timestamp saat ini.
     
    - `system(zip_command)`: Menjalankan perintah zip yang telah diformat menggunakan fungsi system().
     
    - `chdir(timestamp)`: Pindah ke folder timestamp lagi menggunakan fungsi chdir().
     
    - `for (i = 0; i < 15; i++) {`: Melakukan loop sebanyak 15 kali untuk menghapus file-file gambar yang telah didownload sebelumnya.
     
    - `snprintf(filename, MAX_FILENAME_LEN, "%s-%d.jpg", timestamp, i)`: Menggunakan fungsi snprintf() untuk memformat nama file gambar yang akan dihapus, dengan format yang sama seperti saat melakukan download gambar sebelumnya.
     
    - `remove(filename)`: Menghapus file gambar yang telah diformat menggunakan fungsi remove().
     
    - `chdir("..")`: Pindah ke parent directory (directory di atasnya) menggunakan fungsi chdir().
     
    - `rmdir(timestamp)`: Menghapus folder timestamp yang telah dikompres sebelumnya menggunakan fungsi rmdir().

### **2.4 & 2.5**

Kami diminta agar program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri, juga bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete). Berikut ialah langkah pengerjaannya :

- Membuat agar Program Berjalan Secara Daemon 
    
    ```c
    void daemonize() {
    pid_t pid;

    pid = fork();

    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    if (setsid() < 0) {
        exit(EXIT_FAILURE);
    }

    umask(0);

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    openlog("my_daemon", LOG_PID, LOG_DAEMON);
    }
    ```
    penjelasan:
    
    Fungsi daemonize() di atas merupakan suatu implementasi untuk menjadikan sebuah proses sebagai daemon dalam sistem operasi. 

    - `pid_t pid`: Mendeklarasikan variabel bertipe pid_t (identifier untuk suatu proses) dengan nama pid, yang akan digunakan untuk menyimpan nilai dari hasil pemanggilan fungsi fork().
     
    - `pid = fork()`: Menggunakan fungsi fork() untuk membuat sebuah proses anak (child process) dari proses pemanggil (parent process), dan menyimpan nilai kembaliannya dalam variabel pid. 
    
    - `if (pid < 0) { exit(EXIT_FAILURE); }`: Memeriksa apakah pemanggilan fungsi fork() menghasilkan nilai yang kurang dari 0, yang menandakan terjadi kesalahan dalam pembuatan proses anak. 
    
    - `if (pid > 0) { exit(EXIT_SUCCESS); }`: Memeriksa apakah nilai pid adalah lebih besar dari 0, yang menandakan bahwa proses pemanggil adalah proses parent (bukan proses anak). 
     
    - `if (setsid() < 0) { exit(EXIT_FAILURE); }`: Menggunakan fungsi setsid() untuk membuat sebuah session baru dan mengatur proses pemanggil sebagai pemimpin (leader) dari session tersebut. 
     
    - `umask(0)`: Menggunakan fungsi umask() untuk mengatur nilai mask file permission menjadi 0, yang berarti tidak ada pembatasan (full permission) terhadap hak akses file yang akan dibuat oleh daemon.
     
    - `close(STDIN_FILENO)`; `close(STDOUT_FILENO)`; `close(STDERR_FILENO)`: Menutup file descriptor untuk standar input (stdin), standar output (stdout), dan standar error (stderr), yang merupakan langkah untuk memutuskan hubungan antara daemon dengan terminal yang memanggilnya.

- Men-generate Program Killer

    ```c
    void create_killer(char mode, char *namaProgram) {
    FILE *fp;
    fp = fopen("killer.sh", "w");
    if (mode == 'a') {
        fprintf(fp, "#!/bin/bash\n");
        fprintf(fp, "pkill %s\n", namaProgram);
    } else if (mode == 'b') {
        fprintf(fp, "#!/bin/bash\n");
        fprintf(fp, "kill -SIGTERM %d\n", getpid() + 2);
    }
    fprintf(fp, "rm killer.sh\n"); 
    fclose(fp);
    pid_t pid = fork();
    if (pid == 0){
        execl("/bin/chmod", "chmod", "+x", "killer.sh", NULL);
        }
    }
    ```
    Penjelasan:
    
    - `FILE *fp`: Mendeklarasikan sebuah pointer bertipe FILE yang akan digunakan untuk mengakses file killer.sh.

    -  `fp = fopen("killer.sh", "w")`: Membuka file killer.sh dalam mode "write" (w) menggunakan fungsi fopen() dan menyimpan file pointer-nya dalam variabel fp. Jika file sudah ada sebelumnya, isinya akan dihapus.
     
    - `if (mode == 'a') { ... } else if (mode == 'b') { ... }`: Memeriksa nilai dari parameter mode yang diisi oleh karakter 'a' atau 'b'. Jika mode adalah 'a', maka akan dicetak dalam file killer.sh perintah untuk melakukan "pkill" terhadap program dengan nama yang diberikan dalam variabel namaProgram. Jika mode adalah 'b', maka akan dicetak dalam file killer.sh perintah untuk mengirimkan sinyal SIGTERM ke proses dengan PID yang dihasilkan dari penjumlahan antara getpid() dan 2.
     
    - `fprintf(fp, "rm killer.sh\n")`: Menulis perintah "rm killer.sh" ke dalam file killer.sh. Perintah ini akan menghapus file killer.sh setelah perintah kill atau pkill dijalankan.
 
    - `fclose(fp)`: Menutup file pointer fp menggunakan fungsi fclose() setelah selesai menulis pada file killer.sh.
    
    - `pid_t pid = fork()`: Menggunakan fungsi fork() untuk membuat sebuah proses anak (child process) dari proses pemanggil (parent process), dan menyimpan nilai kembaliannya dalam variabel pid.
    
    - `if (pid == 0){ ... }`: Memeriksa apakah nilai pid adalah 0, yang menandakan bahwa ini adalah kode yang dijalankan oleh proses anak (child process). 

- Menambahkan Argumen A / B untuk Mode A & Mode B

    ```c
    int main(int argc, char *argv[]) {
    pid_t pid, sid;

    if (argc < 2) {
        printf("Usage: program a/b\n");
        return 0;
    }

    char *namaProgram = argv[0];
    char mode = *argv[1];
    create_killer(mode, namaProgram);

    ```
    Penjelasan:

    - `pid_t pid, sid`: Mendeklarasikan dua variabel bertipe pid_t, yaitu pid dan sid, yang akan digunakan untuk menyimpan PID (Process ID) dan SID (Session ID) dari proses anak (child process) yang akan dibuat nanti.

    - `if (argc < 2) { ... }`: Memeriksa apakah jumlah argumen baris perintah (argc) kurang dari 2. 
    
    - `char *namaProgram = argv[0]`: Menyimpan nilai dari argumen pertama (argv[0]) ke dalam variabel namaProgram. 
     
    - `char mode = *argv[1]`: Menyimpan nilai dari argumen kedua (argv[1]) ke dalam variabel mode. Argumen kedua dianggap sebagai mode yang akan digunakan dalam fungsi create_killer(). 
     
    - `create_killer(mode, namaProgram)`: Memanggil fungsi create_killer() dengan mengirimkan nilai mode dan namaProgram sebagai argumen.

## Output Soal 2

### **Terminal**
![Alt Text](https://i.ibb.co/gS9WGj1/Screenshot-145.png)

## Soal 3
Ten Hag adalah seorang pelatih Ajax di Liga Belanda. Suatu hari, Ten Hag mendapatkan tawaran untuk menjadi manajer Manchester United. Karena Ten Hag masih mempertimbangkan tawaran tersebut, ia ingin mengenal para pemain yang akan dilatih kedepannya. Dikarenakan Ten Hag hanya mendapatkan url atau link database mentah para pemain bola, maka ia perlu melakukan klasifikasi pemain Manchester United. Bantulah Ten Hag untuk mengenal para pemain Manchester United tersebut hanya dengan 1 Program C bernama “filter.c”
1. Pertama-tama, Program filter.c akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”. Lalu hapus file zip tersebut agar tidak memenuhi komputer Ten Hag.
2. Dikarenakan database yang diunduh masih data mentah. Maka bantulah Ten Hag untuk menghapus semua pemain yang bukan dari Manchester United yang ada di directory.  
3. Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 4 proses yang berbeda. Untuk kategori folder akan menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang.
4. Setelah mengkategorikan anggota tim Manchester United, Ten Hag memerlukan Kesebelasan Terbaik untuk menjadi senjata utama MU berdasarkan rating terbaik dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt dan akan ditaruh di /home/[users]/
Catatan:
- Format nama file yang akan diunduh dalam zip dan isi txt formasi berupa [nama]_[tim]_[posisi]_[rating].png
- Tidak boleh menggunakan system()
- Tidak boleh memakai function C mkdir() ataupun rename().
- Gunakan exec() dan fork().
- Directory “.” dan “..” tidak termasuk yang akan dihapus.
- Untuk poin d DIWAJIBKAN membuat fungsi bernama buatTim(int, int, int), dengan input 3 value integer dengan urutan bek, gelandang, dan striker.

### 3.1
```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>
#include <sys/wait.h>
#include <fnmatch.h>

#define MANUTD_TEAM "ManUtd"
#define MAX_FILENAME 256
```
bagian ini adalah beberapa file header dan mendefinisikan dua konstanta, MANUTD_TEAM dan MAX_FILENAME

```c
void download_Data();
void extract_File();
void remove_non_manchester_united_players();
void sort_player();
void make_team(int bek, int gelandang, int striker);
```
mendefinisikan fungsi yang akan dipakai

```c
int main() {
    download_Data();
    extract_File();
    remove_non_manchester_united_players();
    sort_player();
    
    int bek, gelandang, striker;
    printf("Masukkan jumlah bek: ");
    scanf("%d", &bek);
    printf("Masukkan jumlah gelandang: ");
    scanf("%d", &gelandang);
    printf("Masukkan jumlah striker: ");
    scanf("%d", &striker);
  
    make_team(bek, gelandang, striker);
    return 0;
}
```
pada bagian ini akan memanggil fungsi-fungsi yang dibuat secara beruruta mulai dari download_Data sampai sort_player, setelah sort player akan diminta untuk memasukan jumlah bek, gelandang dan striker. Jika sudah akan memanggil lagi fungsi make_team untuk membuat formasi

```c
void download_Data() {
    char* args[] = {"curl", "-L", "-o", "players.zip", "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", NULL};
    pid_t pid = fork();

    if (pid == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
    } else if (pid == 0) { // child process
        execvp(args[0], args);
        perror("execvp");
        exit(EXIT_FAILURE);
    } else { // parent process
        int status;
        waitpid(pid, &status, 0);
        if (WIFEXITED(status) && WEXITSTATUS(status) != 0) {
            printf("Error downloading players\n");
            exit(EXIT_FAILURE);
        }
    }
}
```
- bagian ini merupakan sebuah fungsi download_Data() yang menggunakan program curl untuk mendownload file "players.zip" dari URL tertentu. Fungsi ini menggunakan fork() untuk membuat child process yang kemudian menjalankan perintah curl menggunakan execvp(). Setelah download selesai, parent process menunggu child process selesai dan memeriksa apakah download berhasil atau tidak.

```c
void extract_File() {
    char* args[] = {"unzip", "players.zip", NULL};
    pid_t pid = fork();

    if (pid == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
    } else if (pid == 0) { // child process
        execvp(args[0], args);
        perror("execvp");
        exit(EXIT_FAILURE);
    } else { // parent process
        int status;
        waitpid(pid, &status, 0);
        if (WIFEXITED(status) && WEXITSTATUS(status) != 0) {
            printf("Error extracting players\n");
            exit(EXIT_FAILURE);
        }
    }

    if (remove("players.zip") != 0) {
        printf("Error deleting file: players.zip\n");
    }
}
```
- bagian ini merupakan sebuah fungsi extract_File() yang bertanggung jawab untuk mengekstrak file "players.zip" menggunakan program unzip. Fungsi ini juga menggunakan fork() untuk membuat child process yang kemudian menjalankan perintah unzip menggunakan execvp(). Setelah ekstraksi selesai, parent process menunggu child process selesai dan memeriksa apakah ekstraksi berhasil atau tidak. Setelah itu, fungsi ini mencoba menghapus file "players.zip" menggunakan remove(). Jika terjadi kesalahan saat menghapus file, fungsi ini akan menampilkan pesan kesalahan.
### 3.2
```c
void remove_non_manchester_united_players() {
    DIR* dir = opendir("players");
    struct dirent* entry;
    char fileName[MAX_FILENAME];

    if (dir == NULL) {
        printf("Error opening directory\n");
        exit(EXIT_FAILURE);
    }

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) { // jika entri adalah file biasa
            if (fnmatch("*_ManUtd_*_*.png", entry->d_name, 0) != 0) {
                sprintf(fileName, "%s/%s", "players", entry->d_name);
                if (unlink(fileName) != 0) {
                    printf("Error deleting file: %s\n", fileName);
                }
            }
        }
    }

    closedir(dir);
}
```
-  bagian inisebuah fungsi yang digunakan untuk menghapus semua file gambar yang tidak terkait dengan pemain sepak bola Manchester United. Pertama-tama, fungsi membuka direktori "players" dan membaca setiap file yang ada di dalamnya. Jika file tersebut merupakan file gambar dan tidak mengandung substring "ManUtd", maka file tersebut akan dihapus. Jika ada kesalahan saat menghapus file, fungsi akan mencetak pesan kesalahan ke layar. Setelah semua file yang tidak perlu dihapus, direktori "players" ditutup.

#### 3.3
```c
void sort_player() {
    DIR * dir = opendir("./players");
    struct dirent * entry;
    char * image_file;
    char * positions[] = {
      "Kiper",
      "Bek",
      "Gelandang",
      "Penyerang"
    };

    if (dir == NULL) {
      printf("Error opening directory\n");
      exit(EXIT_FAILURE);
    }

    int x;
    for (x = 0; x < sizeof(positions) / sizeof(positions[0]); x++) {
        char dir_name[MAX_FILENAME];
        sprintf(dir_name, "./%s", positions[x]);

        if (access(dir_name, F_OK) != 0) {
            char *args[] = {
                "mkdir",
                dir_name,
                NULL
            };
            pid_t pid = fork();

            if (pid == -1) {
                perror("fork");
                exit(EXIT_FAILURE);
            } else if (pid == 0) {
                execvp(args[0], args);
                perror("execvp");
                exit(EXIT_FAILURE);
            } else {
                int status;
                waitpid(pid, &status, 0);
                if (WIFEXITED(status) && WEXITSTATUS(status) != 0) {
                    printf("Error creating directory: %s\n", dir_name);
                    exit(EXIT_FAILURE);
                }
            }
        }
    }

    rewinddir(dir);

    while ((entry = readdir(dir)) != NULL) {
      if (entry -> d_type == DT_REG) { // jika entri adalah file biasa
        image_file = entry -> d_name;
        char * ext = strrchr(image_file, '.');

        if (ext != NULL && (strcmp(ext, ".jpeg") == 0 || strcmp(ext, ".jpg") == 0 || strcmp(ext, ".png") == 0)) {
          int x;
          for (x = 0; x < sizeof(positions) / sizeof(positions[0]); x++) {
            if (strstr(image_file, positions[x]) != NULL) {
              char new_path[MAX_FILENAME];
              sprintf(new_path, "./%s/%s", positions[x], image_file);

              char old_path[MAX_FILENAME];
              sprintf(old_path, "./players/%s", image_file);

              char * args[] = {
                "mv",
                old_path,
                new_path,
                NULL
              };
              pid_t pid = fork();

              if (pid == -1) {
                perror("fork");
                exit(EXIT_FAILURE);
              } else if (pid == 0) {
                execvp(args[0], args);
                perror("execvp");
                exit(EXIT_FAILURE);
              } else {
                int status;
                waitpid(pid, & status, 0);
                if (WIFEXITED(status) && WEXITSTATUS(status) != 0) {
                  printf("Error moving file: %s\n", image_file);
                  exit(EXIT_FAILURE);
                }
              }
              break;
            }
          }
        }
      }
    }

    closedir(dir);

    char * args[] = {
      "rm",
      "-r",
      "./players",
      NULL
    };
    pid_t pid = fork();

    if (pid == -1) {
      perror("fork");
      exit(EXIT_FAILURE);
    } else if (pid == 0) {
      execvp(args[0], args);
      perror("execvp");
      exit(EXIT_FAILURE);
    } else {
      int status;
      waitpid(pid, & status, 0);
      if (WIFEXITED(status) && WEXITSTATUS(status) != 0) {
        printf("Error removing directory: %s\n", "./players");
        exit(EXIT_FAILURE);
      }
    }
}
```
- kode ini akan membaca semua file gambar dalam folder "players", dan memindahkan file gambar tersebut ke folder yang sesuai dengan posisi pemain. Setelah semua file gambar dipindahkan, folder "players" akan dihapus. Proses pengelompokan dilakukan dengan melakukan perulangan untuk setiap posisi pemain yang didefinisikan dalam array "positions". Setiap file gambar yang ditemukan akan dicek untuk melihat apakah terdapat substring posisi pemain dalam namanya. Jika ditemukan, file gambar akan dipindahkan dari folder "players" ke folder yang sesuai dengan posisi pemain menggunakan perintah "mv". Setelah itu, folder "players" akan dihapus menggunakan perintah "rm -r".

### 3.4
```c
void make_team(int bek, int gelandang, int striker) {
  if (bek + gelandang + striker > 10) {
    printf("Formasi tidak bisa dibuat, Pemain terlalu banyak\n");
    return;
  }
  char * positions[] = {
    "Kiper",
    "Bek",
    "Gelandang",
    "Penyerang"
  };

  char * formasi = malloc(sizeof(char) * MAX_FILENAME);
  sprintf(formasi, "/home/testing/Formasi_%d-%d-%d.txt", bek, gelandang, striker);
  FILE * teamFile = fopen(formasi, "w");
  if (teamFile == NULL) {
    printf("Error creating team file: %s\n", strerror(errno));
    return;
  }
  fprintf(teamFile, "Formasi %d-%d-%d\n\n", bek, gelandang, striker);

  int ratings[5][20] = {
    0
  }; // Array untuk menyimpan rating semua pemain
  char playerNames[5][20][MAX_FILENAME]; // Array untuk menyimpan nama semua pemain
  int count[5] = {
    0
  }; // Array untuk menyimpan jumlah pemain di setiap posisi

  // Buka setiap folder posisi dan dapatkan rating dan nama setiap pemain
  for (int x = 0; x < 4; x++) {
    DIR * dir = opendir(positions[x]);
    if (dir == NULL) {
      printf("Error opening directory %s\n", positions[x]);
      continue;
    }
    struct dirent * entry;
    char fileName[MAX_FILENAME];
    while ((entry = readdir(dir)) != NULL) {
      if (entry -> d_type == DT_REG) { // jika entri adalah file biasa
        sprintf(fileName, "%s/%s", positions[x], entry -> d_name);
        char * rating = strrchr(entry -> d_name, '_') + 1; // dapatkan rating dari nama file
        char * name = entry -> d_name;
        name[strlen(name) - strlen(rating) - 1] = '\0'; // hapus peringkat dari nama file
        int ratingVal = atoi(rating);
        ratings[x + 1][count[x + 1]] = ratingVal;
        strcpy(playerNames[x + 1][count[x + 1]], name); // copy nama pemain ke array
        count[x + 1]++;
      }
    }
    closedir(dir);
  }

  // Dapatkan pemain terbaik untuk setiap posisi berdasarkan rating mereka
  int bestKiper = 0;
  int bestBek[bek];
  int bestGelandang[gelandang];
  int bestPenyerang[striker];

  for (int x = 0; x < count[1]; x++) {
    if (ratings[1][x] > ratings[1][bestKiper]) {
      bestKiper = x;
    }
  }

  for (int x = 0; x < bek; x++) {
    bestBek[x] = -1;
  }

  for (int x = 0; x < count[2]; x++) {
    for (int y = 0; y < bek; y++) {
      if (bestBek[y] == -1 || ratings[2][x] > ratings[2][bestBek[y]]) {
        int z;
        for (z = bek - 1; z > y; z--) {
          bestBek[z] = bestBek[z - 1];
        }
        bestBek[y] = x;
        break;
      }
    }
  }

  for (int x = 0; x < gelandang; x++) {
    bestGelandang[x] = -1;
  }

  for (int x = 0; x < count[3]; x++) {
    for (int y = 0; y < gelandang; y++) {
      if (bestGelandang[y] == -1 || ratings[3][x] > ratings[3][bestGelandang[y]]) {
        int z;
        for (z = gelandang - 1; z > y; z--) {
          bestGelandang[z] = bestGelandang[z - 1];
        }
        bestGelandang[y] = x;
        break;
      }
    }
  }

  for (int x = 0; x < striker; x++) {
    bestPenyerang[x] = -1;
  }

  for (int x = 0; x < count[4]; x++) {
    for (int y = 0; y < striker; y++) {
      if (bestPenyerang[y] == -1 || ratings[4][x] > ratings[4][bestPenyerang[y]]) {
        int z;
        for (z = striker - 1; z > y; z--) {
          bestPenyerang[z] = bestPenyerang[z - 1];
        }
        bestPenyerang[y] = x;
        break;
      }
    }
  }

  // Print pemain terbaik untuk setiap posisi ke file tim
  fprintf(teamFile, "Kiper: %s (%d)\n", playerNames[1][bestKiper], ratings[1][bestKiper]);
  fprintf(teamFile, "Bek:\n");
  for (int x = 0; x < bek; x++) {
    fprintf(teamFile, "- %s (%d)\n", playerNames[2][bestBek[x]], ratings[2][bestBek[x]]);
  }
  fprintf(teamFile, "Gelandang:\n");
  for (int x = 0; x < gelandang; x++) {
    fprintf(teamFile, "- %s (%d)\n", playerNames[3][bestGelandang[x]], ratings[3][bestGelandang[x]]);
  }
  fprintf(teamFile, "Penyerang:\n");
  for (int x = 0; x < striker; x++) {
    fprintf(teamFile, "- %s (%d)\n", playerNames[4][bestPenyerang[x]], ratings[4][bestPenyerang[x]]);
  }

  fclose(teamFile);
  free(formasi);
}
```
pertama-tama pada kode ini  akan mengecek apakah jumlah total pemain melebihi 10 atau tidak. Jika ya, program akan menampilkan pesan "Formasi tidak bisa dibuat, Pemain terlalu banyak" dan keluar dari fungsi make_team. Jika jumlah total pemain kurang dari atau sama dengan 10, program akan membuat sebuah file baru di direktori /home/testing/ dengan format nama Formasi_{bek}-{gelandang}-{striker}.txt dan menuliskan formasi tim ke dalam file tersebut. Selanjutnya, program akan membaca file-file yang terdapat pada direktori /Kiper, /Bek, /Gelandang, dan /Penyerang. Setiap file pada folder tersebut berisi nama pemain dan rating pemain, dengan format nama_pemain_rating.txt. Rating pemain dalam rentang 0 hingga 99. Program akan menyimpan nama dan rating pemain ke dalam array playerNames dan ratings pada indeks yang sesuai dengan posisi pemain.
Setelah itu, program akan mencari pemain terbaik untuk setiap posisi dengan membandingkan rating pemain pada array ratings. Pemain terbaik akan disimpan pada array bestKiper, bestBek, bestGelandang, dan bestPenyerang. Untuk setiap posisi, program akan mencari pemain terbaik sebanyak jumlah pemain pada posisi tersebut. Hasil pencarian pemain terbaik akan disimpan pada indeks dari array bestBek, bestGelandang, dan bestPenyerang yang masih bernilai -1. Apabila sudah terdapat pemain terbaik pada indeks tersebut, maka pemain terbaik baru akan ditempatkan pada posisi yang tepat di antara pemain terbaik lainnya.Setelah itu, program akan menuliskan nama pemain terbaik untuk setiap posisi ke dalam file tim yang telah dibuat. Pemain terbaik akan diurutkan sesuai dengan posisi masing-masing, mulai dari kiper hingga penyerang.

###output soal 3

### **Terminal**
<a href="https://ibb.co/zfHMqSn"><img src="https://i.ibb.co/h8163K7/Screenshot-2023-04-08-212618.png" alt="Screenshot-2023-04-08-212618" border="0"></a>

### **Output Folder posisi.txt**
<a href="https://ibb.co/rp65htN"><img src="https://i.ibb.co/R60QKSd/Screenshot-2023-04-08-213210.png" alt="Screenshot-2023-04-08-213210" border="0"></a>

### **Output formasi.txt**
<a href="https://ibb.co/7r9NsWr"><img src="https://i.ibb.co/SVL7FnV/Screenshot-2023-04-08-212228.png" alt="Screenshot-2023-04-08-212228" border="0"></a>

## Soal 4

Banabil adalah seorang mahasiswa yang rajin kuliah dan suka belajar. Namun naasnya Banabil salah mencari teman, dia diajak ke toko mainan oleh teman-temannya dan teracuni untuk membeli banyak sekali mainan dan kebingungan memilih mainan mana yang harus dibeli. Hal tersebut menyebabkan Banabil kehilangan fokus dalam pengerjaan tugas-tugas yang diberikan oleh dosen nya. Untuk mengembalikan fokusnya, Banabil harus melatih diri sendiri dalam membuat **program untuk menjalankan script bash yang menyerupai crontab dan menggunakan bahasa C** karena baru dipelajari olehnya. Untuk menambah tantangan agar membuatnya semakin terfokus, Banabil membuat beberapa ketentuan custom yang harus dia ikuti sendiri. Ketentuan tersebut berupa:

- Banabil tidak ingin menggunakan fungsi system(), karena terlalu mudah.
- Dalam pelatihan fokus time managementnya, Banabil harus bisa membuat program yang dapat menerima argumen berupa **Jam** (0-23), **Menit** (0-59), **Detik** (0-59), **Tanda asterisk [ * ]** (value bebas), serta **path file** **.sh**.
- Dalam pelatihan fokus untuk ketepatan pilihannya, Banabil ingin programnya dapat mengeluarkan **pesan “error”** apabila argumen yang diterima program tidak sesuai. **Pesan error dapat dibentuk sesuka hati oleh pembuat program. terserah bagaimana, yang penting tulisan error**.
- Terakhir, dalam pelatihan kesempurnaan fokusnya, Banabil ingin program ini **berjalan dalam background** dan **hanya menerima satu config cron**.
- Bonus poin apabila CPU state minimum.

**Contoh untuk run**: /program \* 44 5 /home/Banabil/programcron.sh

### **4.1**

Kami diminta untuk tidak boleh menggunakan fungsi system(), karena dianggap terlalu mudah. Kami diminta untuk membuat program untuk menjalankan script bash yang menyerupai crontab dan menggunakan bahasa C  dengan ketentuan pertama yaitu tidak boleh menggunakan fungsi system(), karena dianggap terlalu mudah. Oleh karena itu, dalam program di bawah ini tidak ada yang menggunakan fungsi ` system()`  

### **4.2**

Selanjutnya, kami diminta untuk membuat program tersebut dengan ketetuan argumen input  berupa **Jam** (0-23), **Menit** (0-59), **Detik** (0-59), **Tanda asterisk [ * ]** (value bebas), serta **path file** **.sh**. Berikut langkah pengerjaannya : 

- Input Argumen
    
    ```c
    #include <stdio.h>
    #include <stdlib.h>
    #include <time.h>
    #include <unistd.h>
    #include <string.h>
    #include <stdbool.h>
    #include <sys/wait.h>
    #include <sys/types.h>
    ```
    
    - Bagian ini berisi beberapa `#include` yang akan dibutuhkan untuk menjalankan program.
    
    ```c
    int string_to_int(char *str) {
        if (strcmp(str, "*") == 0) {
            return -1;
        }
        return atoi(str);
    }
    ```
    
    - Ini adalah fungsi bantuan yang mengonversi sebuah string ke integer. Jika string input adalah karakter asterisk (`*`), maka fungsi akan mengembalikan `-1`. Jika tidak, maka fungsi akan mengubah string tersebut menjadi integer menggunakan fungsi `atoi` dan mengembalikan hasilnya.
    
    ```c
    int main(int argc, char *argv[]) {
        //...
    }
    ```
    
    - Fungsi `main` adalah tempat program mulai dieksekusi. Fungsi ini menerima argumen baris perintah sebagai input dengan menggunakan variabel `argc` dan `argv[]`.

### **4.3**

Kami diminta untuk mengeluarkan **pesan “error”** apabila argumen yang diterima program tidak sesuai. **Pesan error dapat dibentuk sesuka hati oleh pembuat program. terserah bagaimana, yang penting tulisan error**. Berikut langkah pengerjaannya : 

- Proses Input Error
    
    ```c
    if (argc != 6) {
        fprintf(stderr, "Usage: %s <hour> <minute> <second> <asterisk> <script>\n", argv[0]);
        return 1;
    }
    ```
    
    - Program memerlukan tepat enam argumen untuk dijalankan, dengan contoh  `./mainan 8 30 59 \* /home/kali/Dcouments/Shift2/test2.sh` . Jika jumlah argumen yang diberikan tidak sama dengan enam, maka pesan kesalahan akan ditampilkan ke stderr dan program akan mengembalikan 1, menandakan terjadi kesalahan.
    
    ```c
    int hour = string_to_int(argv[1]);
    int minute = string_to_int(argv[2]);
    int second = string_to_int(argv[3]);
    char *asterisk = argv[4];
    char *script = argv[5];
    ```
    
    - Kode ini mengonversi argumen-argumen menjadi tipe data yang sesuai, yaitu data jam, menit, dan detik menjadi integer dengan menggunakan fungsi `string_to_int`, sedangkan asterisk dan nama script menjadi string.
    
    ```c
    if (hour < 0 || hour > 23 || minute < 0 || minute > 59 || second < 0 || second > 59) {
        fprintf(stderr, "Error: Invalid argument.\n");
        return 1;
    }
    ```
    
    - Program akan memeriksa apakah argumen input untuk jam, menit, dan detik berada dalam rentang valid (jam: 0-23, menit/detik: 0-59), dan akan menampilkan pesan kesalahan jika salah satu dari mereka tidak valid.
    
    ```c
    printf("Cron configuration:\n");
    printf("Hour: %d\n", hour);
    printf("Minute: %d\n", minute);
    printf("Second: %d\n", second);
    printf("Asterisk: %s\n", asterisk);
    printf("Script: %s\n", script);
    ```
    
    - Pada bagian ini mencetak informasi tentang pekerjaan cron yang telah diinput.

### **4.4**

Kami diminta untuk membuat program ini **berjalan dalam background** dan **hanya menerima satu config cron**. Berikut langkah pengerjaannya : 

- Proses Cron
    
    ```c
    pid_t pid = fork();
    if (pid == 0) {
        // child process
        //...
    } else if (pid == -1) {
        // parent process
        //...
    }
    ```
    
    - Program membuat child process menggunakan sistem panggilan `fork()`. Proses induk menerima PID dari proses anak, yang disimpan dalam variabel `pid`.
    
    ```c
    while (true) {
                time_t current_time = time(NULL);
                struct tm *local_time = localtime(&current_time);
    
                // menunggu waktu yang sesuai
                if ((hour == -1 || local_time->tm_hour == hour) &&
                    (minute == -1 || local_time->tm_min == minute) &&
                    (second == -1 || local_time->tm_sec == second)) {
                    // menjalankan script bash
                    pid_t pid_script = fork();
                    if (pid_script == 0) {
                        execl("/bin/bash", "bash", script, NULL);
                        // mengeluarkan pesan error 
                        fprintf(stderr, "Error: Failed to run script.\n");
                        exit(1);
                    } else if (pid_script == -1) {
                        // mengeluarkan pesan error 
                        fprintf(stderr, "Error: Failed to fork process.\n");
                        exit(1);
                    } else {
                        // parent process
                        int status;
                        waitpid(pid_script, &status, 0);
                        if (WIFEXITED(status)) {
                            printf("Script exited with status %d\n", WEXITSTATUS(status));
                        } else {
                            printf("Script terminated abnormally\n");
                        }
                        if (strcmp(asterisk, "*") != 0) {
                            break;
                        }
                    }
                }
    
                // menunggu 1 detik
                sleep(1);
            }
    ```
    
    - Proses anak mengeksekusi loop yang tak terbatas, yang berulang kali memeriksa waktu saat ini menggunakan `time()` dan `localtime()`. Lalu akan membandingkan jam, menit, dan detik saat ini dengan nilai yang ditentukan dari argumen masukan. Jika mereka cocok, maka proses anak akan menjalankan script bash yang telah ditentukan menggunakan `fork()` dan `execl()`. Setelah itu, proses anak akan menunggu script selesai dieksekusi menggunakan `waitpid()`, lalu mencetak status keluaran dari script tersebut.
    - Jika argumen asterisk tidak sama dengan `"*"`, maka proses anak akan keluar dari loop tak terbatas dan berhenti.
    - Fungsi `sleep()` digunakan untuk memberikan jeda program selama 1 detik antara setiap pengecekan waktu.
    - Proses induk mencetak pesan yang menunjukkan bahwa program sedang berjalan di background dengan PID-nya. Setelah itu, proses induk kembali ke prompt baris perintah.
    
    ```c
    } else if (pid == -1) {
            // mengeluarkan pesan error
            fprintf(stderr, "Error: Failed to fork process.\n");
            return 1;
        } else {
            // parent process
            printf("Program is running in the background with PID %d.\n", pid);
        }
    ```
    
    - Program menangani kesalahan yang terjadi selama proses eksekusi. Jika terjadi kesalahan pada saat melakukan fork atau pada saat menjalankan script, maka program akan menghasilkan pesan kesalahan dan akan mengembalikan 1, menandakan terjadinya kesalahan.

### **4.5**

Terakhir, kami diminta untuk membuat program dengan CPU state minimum. Hal tersebut dapat dicek menggunakan comman `ps aux` atau `htop`

## Output Soal 4

### **Terminal**
![Alt Text](https://i.ibb.co/3mG8Fzx/Screenshot-2023-04-08-184743.png)

### **File Explorer**
![Alt Text](https://i.ibb.co/xDZLXL7/Screenshot-2023-04-08-184805.png)

### **CPU State Minimum**
![Alt Text](https://i.ibb.co/34S1DZL/Screenshot-2023-04-08-184906.png)

