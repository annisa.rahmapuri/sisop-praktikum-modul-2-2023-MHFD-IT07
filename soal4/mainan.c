#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <sys/wait.h>
#include <sys/types.h>

// membuat fungsi untuk mengubah string asterisk menjadi integer 0
int string_to_int(char *str) {
    if (strcmp(str, "*") == 0) {
        return -1;
    }
    return atoi(str);
}

int main(int argc, char *argv[]) {
    // mengecek jumlah argumen yang diinput
    if (argc != 6) {
        fprintf(stderr, "Usage: %s <hour> <minute> <second> <asterisk> <script>\n", argv[0]);
        return 1;
    }

    // inisialiasi untuk mengubah tipe data dengan fungsi di atas -> string_to_int
    int hour = string_to_int(argv[1]);
    int minute = string_to_int(argv[2]);
    int second = string_to_int(argv[3]);
    char *asterisk = argv[4];
    char *script = argv[5];

    // melakukan pengecekan terhadap argumen yang diinput
    if (hour < 0 || hour > 23 || minute < 0 || minute > 59 || second < 0 || second > 59) {
        fprintf(stderr, "Error: Invalid argument.\n");
        return 1;
    }

    // menampilkan cron configuration yang diinput
    printf("Cron configuration:\n");
    printf("Hour: %d\n", hour);
    printf("Minute: %d\n", minute);
    printf("Second: %d\n", second);
    printf("Asterisk: %s\n", asterisk);
    printf("Script: %s\n", script);

    // melakukan proses fork untuk child process
    pid_t pid = fork();
    if (pid == 0) {
        // child process
        while (true) {
            time_t current_time = time(NULL);
            struct tm *local_time = localtime(&current_time);

            // menunggu waktu yang sesuai
            if ((hour == -1 || local_time->tm_hour == hour) &&
                (minute == -1 || local_time->tm_min == minute) &&
                (second == -1 || local_time->tm_sec == second)) {
                // menjalankan script bash
                pid_t pid_script = fork();
                if (pid_script == 0) {
                    execl("/bin/bash", "bash", script, NULL);
                    // mengeluarkan pesan error 
                    fprintf(stderr, "Error: Failed to run script.\n");
                    exit(1);
                } else if (pid_script == -1) {
                    // mengeluarkan pesan error 
                    fprintf(stderr, "Error: Failed to fork process.\n");
                    exit(1);
                } else {
                    // parent process
                    int status;
                    waitpid(pid_script, &status, 0);
                    if (WIFEXITED(status)) {
                        printf("Script exited with status %d\n", WEXITSTATUS(status));
                    } else {
                        printf("Script terminated abnormally\n");
                    }
                    if (strcmp(asterisk, "*") != 0) {
                        break;
                    }
                }
            }

            // menunggu 1 detik
            sleep(1);
        }
    } else if (pid == -1) {
        // mengeluarkan pesan error
        fprintf(stderr, "Error: Failed to fork process.\n");
        return 1;
    } else {
        // parent process
        printf("Program is running in the background with PID %d.\n", pid);
    }

    return 0;
}
